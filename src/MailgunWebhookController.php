<?php

namespace Minioak\Api\Webhooks;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Response;

class MailgunWebhookController extends Controller
{

    /**
     * Handle the Mailgun webhook and call
     * method if available
     *
     * @return Response
     */
    public function handleWebHook(Request $request)
    {
        if ($this->validateSignature($request)) {
            $event = $request->input('event');

            $method = 'handle' . studly_case(str_replace('.', '_', $event));

            if (method_exists($this, $method)) {
                $this->{$method}($request);
            }

            return new Response;
        }

        return new Response('Unauthorized', 401);
    }

    /**
     * Validates the signature of a mandrill request if key is set
     *
     * @param  Request $request
     * @param  string  $webhook_key
     *
     * @return bool
     */
    private function validateSignature(Request $request)
    {
        $webhook_key = config('mailgun-webhooks.webhook_key');

        return $this->verify($webhook_key, $request->input('token'), $request->input('timestamp'), $request->input('signature'));
    }

    /**
     * Generates a base64-encoded signature for a Mandrill webhook request.
     * @param string $apiKey the webhook's authentication key
     * @param string $token the token passed by the webhook
     * @param int $timestamp the timestamp passed by the webhook
     * @param string $signature the signature passed by the webhook
     */
    public function verify($apiKey, $token, $timestamp, $signature)
    {
        //check if the timestamp is fresh
        if (time()-$timestamp>15) {
            return false;
        }
        
        //returns true if signature is valid
        return hash_hmac('sha256', $timestamp.$token, $apiKey) === $signature;
    }
}
